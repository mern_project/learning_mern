const {MongoClient} = require('mongodb');


 async function FetchImages(){
	const client = new MongoClient("mongodb://mongo:27017");
	let data = [];
	try{
		await client.connect();
		const database =  client.db("MERN_PROJECT");
		const collection = database.collection("Docker_Images");
		const cursor = collection.find({},{projection:{_id:0}});
		const images = await cursor.toArray();
		data = images.map(e => e.image);
	
	}
	catch(e) { console.error(e); }
	finally {
	  await client.close();
  	}
return data;
}

//FetchImages().catch(console.error);
module.exports = FetchImages;
