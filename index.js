const express = require("express");
const { exec } = require("child_process");
const  FetchImages = require("./mongo");
const app = express();
app.get("/runform", (req, res) => {
  res.sendFile(__dirname + "/rundocker.html");
});


app.get('/images', async (req , res) => {
	const data = await FetchImages();	
	res.send(data)
});

app.get("/terminal", (req, res) => {
  res.sendFile(__dirname + "/terminal.html");
})


app.get("/terminalReq", (req, res) => {
  const cname = req.query.terminalQ;
  exec(
    `${cname}`,
    (err, stdout, stderr) => {
      console.log(err);
      console.log(stdout);
      console.log(stderr);
      if (err || stderr) {
        res.send("Error executing command.." + stdout);
              return;

      }else{
      res.send( stdout);}
    }
  );
});





app.get("/run", (req, res) => {
  const cname = req.query.cname;
  const cimage = req.query.cimage;
  exec(
    ` docker run -d --name  ${cname} -i -t  ${cimage}`,
    (err, stdout, stderr) => {
      console.log(err);
      console.log(stdout);
      console.log(stderr);
      if (err || stderr) {
        res.send("Error Launching Container.." + stdout);
	      return;
    
      }else{
      res.send(stdout.substring(0,12));}
    }
  );
});
app.get("/ps", (req, res) => {
	let data = []

  exec(" docker ps | tail -n +2", (err, stdout, stderr) => {
    let a = stdout.split("\n");
	  console.log(a)
    
    a.forEach((cdetails) => {
      var cinfo = cdetails.trim().split(/\s+/);
      console.log(cinfo[0] + " " + cinfo[1] + " " + cinfo[2]);
     data.push( 
	     {"Id":cinfo[0],
	     "imageName":cinfo[1],
	     "Command":cinfo[2],
	     "containerName":cinfo[cinfo.length - 1]
     });
    
  });
	  console.log(data);
	  res.send(data);
});
});

app.listen(4001, () => {
  console.log("container app tool started...");
});

